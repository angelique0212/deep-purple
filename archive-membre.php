<?php get_header(); ?>

<?php
if(have_posts()){
    while(have_posts()){
        the_post();
            ?>
                <div class="container mt-4">
                    <div class="jumbotron">
                        <h1 class="text-center">Membres</h1>
                    </div>
                    <div class="row">
                        <div class="mb-4 col-md-4">
                            <div class="card">
                            <?php the_post_thumbnail(); ?>
                                <div class="card-body">
                                <img src="https://via.placeholder.com/300x300" class="card-img-top" alt="">
                
                                    <p class="card-text"><?php the_title(); ?></p>
                                    <div class="btn-group">
                                    <?php echo "<a href='".get_permalink()."'><class='btn btn-sm btn-outline-success'>View</a>"; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
    }
}
?>

<?php get_footer(); ?>